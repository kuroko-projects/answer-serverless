export type UserRecord = {
  lineId: string;
  displayName: string;
  followedAt: number;
  invitedBy: string;
  invite: string[];
  birthday: string;
  birthyear: string;
};
